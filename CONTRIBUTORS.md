### Project head
Bernhard Y. Renard \<RenardB(at)rki.de\>  
  
### Technical head
Oliver Drechsel \<DrechselO(at)rki.de\>
  
### Active Contributors
Paul Wolk \<WolkP(at)rki.de\>
Andreas Andrusch \<AndruschA(at)rki.de\>

### Former Contributors
Sarah M. Bastian \<Blinzi.Bastian(at)gmail.com\>
 
