#!/usr/bin/env python3

import argparse
import os
import sys

from IDeFIX.bclreader import BclReader
from IDeFIX.path import Path
from IDeFIX.utilities import DemuxException, Report
from IDeFIX.files import FilterFile, SampleSheet, RunInfo


def parse():
    parser = argparse.ArgumentParser(prog="IDeFIX", description="Index abundance report from Illumina NGS raw data "
                                                                "and Sample Sheet correction")
    parser.add_argument("projectpath", help="The path of the (top-level) project folder.")
    parser.add_argument("-j", "--jobs", type=int, default=20, help="Number of jobs/processes. (default: 20)")
    parser.add_argument("-t", "--threshold", type=int, default=1000,
                        help="Threshold for minimum number of reads of an index to be reported. "
                             "(default: 1000)")
    parser.add_argument("-s", "--samplesheet_name", default="SampleSheet.csv",
                        help="Defines the location of the Sample Sheet.")
    parser.add_argument("-S", "--samplesheet_output", default="CorrectedSampleSheet.csv",
                        help="Defines the location of the corrected SampleSheet.")
    parser.add_argument("-c", "--correct_chars", type=flag2bool, default=True,
                        help="If True, removes backslashes, dots, tabs and whitespaces as well as umlauts from "
                             "SampleSheet and saves the corrected version as SampleSheet.csv and the original as "
                             "SampleSheet_beforeIDeFIX.csv.\n"
                             "Subsequent actions are then performed with the corrected Sample Sheet (default: True)")
    parser.add_argument("-C", "--only_char_correct", type=flag2bool, default=False,
                        help="Removal of undesired characters (backslashes, dots, tabs and whitespaces as well as "
                             "umlauts) only. (default: False)")
    return parser


def flag2bool(user_input):
    if user_input.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif user_input.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    parser = parse()
    args = parser.parse_args()
    print("\n*** REPORT ***\nParsed arguments: ", str(args))
    if (args.samplesheet_name.endswith(".csv")):
        sampath = args.samplesheet_name
    else:
        sampath = args.samplesheet_name + ".csv"
    if (args.samplesheet_output.endswith(".csv")):
        oppath = args.samplesheet_output
    else:
        oppath = args.samplesheet_output + ".csv"
    if (args.correct_chars == True):
        if (sampath==oppath):
            oppath = "Corrected"+oppath
    else:
        oppath = sampath
    path = Path(args.projectpath, sampath, oppath)
    # storing backup of original Sample Sheet and corrected Sample Sheet after removal of unwanted characters
    # in project folder if -c is True:
    if args.correct_chars:
        orig_samplesheet = SampleSheet.read_samplesheet_linewise(path.samplesheet)  # stores samplesheet linewise
        #SampleSheet.backup_original_samplesheet(orig_samplesheet, path.project)  # writes SampleSheet_beforeIDeFIX.csv
        SampleSheet.write_pre_corrected_samplesheet(SampleSheet.check_and_replace_chars(orig_samplesheet), path.corrsamplesheet)
        print("\n... PROGRESS ...\nUnwanted characters in the Sample Sheet were removed")
    # exit if only undesired characters in Sample Sheet should be corrected (-C is True):
    if args.only_char_correct:
        sys.exit(0)

    #sys.exit(0)
    
    runinfo = RunInfo(path.runinfo)
    #print(path.filterfiles_list)
    #print(args.jobs)
    filterfile = FilterFile(path.filterfiles_list, args.jobs)
    print("\n... PROGRESS ...\nThe Sample Sheet, Run Info and Filter File were read.\n"
          "Data from the BCL files are about to be processed.")
    bcl_reader = BclReader(path.lanes_list, runinfo.indexranges, filterfile.filter_dict, args.jobs, args.threshold)
    print("\n... PROGRESS ...\nIndices and their abundances were snooped from the BCL raw data.")

    samplesheet = SampleSheet(path.corrsamplesheet)
    SampleSheet.reversecomplement(path.corrsamplesheet)
    revcomsamplesheet = SampleSheet(path.corrsamplesheet+".rc")

    # Report
    report = Report(path.project, bcl_reader.indexcombi_abundance_descending_df, samplesheet.indices_sampleID_df,
                    revcomsamplesheet.indices_sampleID_df, samplesheet.index_repeats)
    report.write_csv_report()
    print("\n\n*** INDEX REPORT[:15] ***\n", report.index_report_df[:15])
    print("\nThe whole INDEX REPORT about the existing Indices/Barcodes is now available at\n"
          "{}".format(os.path.join(path.project, "IDeFIX_Report.csv\n\n")))
    print("\n******* FINAL REPORT *******\n\n")
    # In case: indices present in Sample Sheet, but missing in raw data
    if report.missing_idx_dict:
        #here check if revcom is necessary
        if report.revcom_missing_idx_dict:
            report.problem("\nThere are indices in the Sample Sheet, which are missing in the raw data:\n" +
                        str(report.missing_idx_dict) + "\n{Index(-combination): corresponding Sample ID}\n----------\n")
        else:
            report.problem("\nThere are indices in the Sample Sheet, which are missing in the raw data:\n" +
                        str(report.missing_idx_dict) + "\n{Index(-combination): corresponding Sample ID}\nThis can be fixed if you reverse-complement the samplesheet.\n----------\n")
    else:
        print("Every index(-combination) in the Sample Sheet is present in the raw data as well.\n\n")
    # In case: gap in the abundance of indices
    if report.idx_abundance_not_fitting:
        print("There is a gap in the actual abundance of the indices in the Sample Sheet.\n"
              "Meaning, there are either indices present in great quantity, which are not in the Sample Sheet,\n"
              "or there are indices in the Sample Sheet which are present in low abundance. \n"
              "For detailed information, please see IDeFIX_Report.csv.\n\n")
    else:
        print("All indices in the Sample Sheet are those with the highest actual abundance in the raw data.\nMeaning, "
              "there are neither indices present in great quantity, which are not in the Sample Sheet,\nnor are there "
              "indices in the Sample Sheet which are present in low abundance.\n"
              "For detailed information, please see IDeFIX_Report.csv.\n\n")
    # In case: index(-combination) repetitions in Sample Sheet
    if not samplesheet.index_repeats:
        print("Each index(-combination) is unique for a certain Sample ID.\n\n")
    elif samplesheet.index_repeats:  # if there are index repeats (dict not empty)
        report.problem("\nThere are Index Repeats:\n" + str(samplesheet.index_repeats) +
                       "\n{Index(-combination): corresponding Sample IDs}\n\n")
    # if there were severe problems (problem count != 0)
    if report.problem_count:       
        open(os.path.join(args.projectpath, "idefix_failed.txt"), "a").close()
        sys.exit(1)
    else:
        open(os.path.join(args.projectpath, "idefix_completed.txt"), "a").close()


if __name__ == "__main__":
    try:
        main()
        sys.exit(0)
    except DemuxException as exception:
        open(os.path.join(args.projectpath, "idefix_failed.txt"), "a").close()
        print("\n--- ERROR ---\n" + exception.message)
        sys.exit(1)
