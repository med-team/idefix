import os
import pprint
import sys 

class DemuxException(Exception):
    def __init__(self, message):
        self.message = message


class Report:
    """
    Takes the project path, a pandas-DataFrame with the indices/index combinations in descending order and their amount,
    a pandas.DataFrame with the indices/index combinations and the Sample IDs, as well as
    a dictionary of index repetitions in the Sample Sheet {keys: index(-combination), values: list of SampleIDs}.
    The method make_report_df() creates a Report pandas.DataFrame, which is stored self.index_report_df and written
    to IDeFIX_Report.csv.
    The attribute self.problem_count takes track of problems found by IDeFIX.
    The attribute self.idx_abundance_not_fitting stores a boolean recording if there are outliers of indices from the
    Sample Sheet in the index report DataFrame.
    The attribute self.missing_idx_dict records indices from the Sample Sheet, which are missing in the raw data
    (or are below the user defined threshold for minimum amount of indices to be recorded)
    {keys: missing index(-combination), values: corresponding Sample ID}.

    """
    def __init__(self, projectpath, index_abundance_df, index_sampleID_df, index_rcsampleID_df, index_repeats):
        self.path = projectpath
        self.index_abundance_df = index_abundance_df        # from .bcl files = raw data
        self.index_sampleID_df = index_sampleID_df          # from Sample Sheet
        self.index_rcsampleID_df = index_rcsampleID_df
        self.index_repeats = index_repeats
        self.index_report_df, self.idx_abundance_not_fitting, self.missing_idx_dict = self.make_report_df(0)
        self.revcom_missing_idx_dict = {"notEmpty"}
        if self.missing_idx_dict:
            binone, bintwo, self.revcom_missing_idx_dict = self.make_report_df(1)
        self.problem_count = 0

    def make_report_df(self, mode):
        """
        Makes an Index Report DataFrame containing the existent indices, their abundance, and if they are in the
        Sample Sheet, the corresponding Sample ID and repetitions in the Sample Sheet.
        """
        if mode == 0:
            sample = self.index_sampleID_df
        elif mode == 1:
            sample = self.index_rcsampleID_df
        else: 
            print("Mode not set correctly \n\n")
        # to create a new DataFrame with: Existent Indices | Quantity | Index in Sample Sheet | Sample ID
        index_report_df = self.index_abundance_df
        index_report_df["Index in Sample Sheet"] = 0
        index_report_df["Sample ID"] = ""
        # to check if the existent Indices (from raw data) is in the Sample Sheet and if so, to add this information
        # as well as the Sample ID and to record outliers:
        idx_abundance_not_fitting = False                   # outlier indices
        missing_idx_dict = {}
        for ss_idx in range(len(sample["Indices"])):
            missing_idx_dict[sample.ix[ss_idx, "Indices"]] \
                = sample.ix[ss_idx, "Sample_ID"]
        for ex_idx in range(len(index_report_df["Existent Indices"])):          # for each existing index from raw data
            for ss_idx in range(len(sample["Indices"])):        # for each index from the Sample Sheet
                #print (index_report_df.ix[ex_idx, "Existent Indices"], " ", self.index_sampleID_df.ix[ss_idx, "Indices"])
                
                samplestring = sample.ix[ss_idx, "Indices"]
                reportstring = index_report_df.ix[ex_idx, "Existent Indices"]

                i = 16
                if samplestring[7] == "N":
                    samplestring = samplestring[:7]+samplestring[-9:]
                    reportstring = reportstring[:7]+reportstring[-9:]
                    i = 15

                if samplestring[i] == "N":
                    samplestring = samplestring[:-1]
                    reportstring = reportstring[:-1]
                    
                if reportstring == samplestring:
                    index_report_df.ix[ex_idx, "Index in Sample Sheet"] += 1
                    if ex_idx > 0:
                        if index_report_df.ix[ex_idx-1, "Index in Sample Sheet"] == 0:
                            idx_abundance_not_fitting = True                    # if previous index was not in the
                                                                                        # Sample Sheet, but this one is
                    if len(index_report_df.ix[ex_idx, "Sample ID"]) > 0 and not \
                        str(index_report_df.ix[ex_idx, "Sample ID"]) == \
                        str(sample.ix[ss_idx, "Sample_ID"]):
                        index_report_df.ix[ex_idx, "Sample ID"] \
                        += ", " + str(sample.ix[ss_idx, "Sample_ID"])
                    elif len(index_report_df.ix[ex_idx, "Sample ID"]) == 0:
                        index_report_df.ix[ex_idx, "Sample ID"] += str(sample.ix[ss_idx, "Sample_ID"])
                    if sample.ix[ss_idx, "Indices"] in missing_idx_dict.keys():
                        del missing_idx_dict[sample.ix[ss_idx, "Indices"]]
                #else:
                #    if index_report_df.ix[ex_idx, "Existent Indices"] == self.index_sampleID_df.ix[ss_idx, "Indices"]:
                #        index_report_df.ix[ex_idx, "Index in Sample Sheet"] += 1
                #        if ex_idx > 0:
                #            if index_report_df.ix[ex_idx-1, "Index in Sample Sheet"] == 0:
                #                idx_abundance_not_fitting = True                    # if previous index was not in the
                #                                                                    # Sample Sheet, but this one is
                #        if len(index_report_df.ix[ex_idx, "Sample ID"]) > 0 and not \
                #                str(index_report_df.ix[ex_idx, "Sample ID"]) == \
                #                str(self.index_sampleID_df.ix[ss_idx, "Sample_ID"]):
                #            index_report_df.ix[ex_idx, "Sample ID"] \
                #                += ", " + str(self.index_sampleID_df.ix[ss_idx, "Sample_ID"])
                #        elif len(index_report_df.ix[ex_idx, "Sample ID"]) == 0:
                #            index_report_df.ix[ex_idx, "Sample ID"] += str(self.index_sampleID_df.ix[ss_idx, "Sample_ID"])
        # to record indices in the Sample Sheet, which are not in the raw data or below the threshold: 
        return index_report_df, idx_abundance_not_fitting, missing_idx_dict

    def write_csv_report(self):
        self.index_report_df.to_csv(os.path.join(self.path, "IDeFIX_Report.csv"))

    def problem(self, message):
        print("!!! PROBLEM !!!", message)
        self.problem_count += 1
