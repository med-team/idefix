import os
from re import match
from IDeFIX.utilities import DemuxException


class Path:
    """
    Takes path project directory for instantiation.
    Contains all the necessary paths of a project and checks if every directory and file is in place and
    readable.
    """
    def __init__(self, projectpath, samplesheet, corrsamplesheet):
        self.project = self.dir_check(projectpath)
        self.samplesheet = self.file_check(os.path.join(projectpath, samplesheet))
        self.corrsamplesheet = (os.path.join(projectpath, corrsamplesheet))
        self.runinfo = self.file_check(os.path.join(projectpath, "RunInfo.xml"))
        self.basecalls = self.dir_check(os.path.join(projectpath, "Data", "Intensities", "BaseCalls"))
        self.lanes_list = [self.dir_check(os.path.join(self.basecalls, str(dir))) for dir in os.listdir(self.basecalls)
                           if match("L[0-9][0-9][0-9]", dir)]
        self.filterfiles_list = self.find_filterfile_paths()

    def find_filterfile_paths(self):
        """
        Returns all Filter File paths in a List after collecting them by searching through the paths of Lanes
        in self.lanes_list.
        """
        filterfilepaths = []
        for lanepath in self.lanes_list:
            filterfilepaths += [os.path.join(lanepath, filterfile) for filterfile in
                                [file for file in os.listdir(lanepath) if file.endswith(".filter")]]
        return filterfilepaths

    def file_check(self, path2file):
        """
        Raises DemuxException if file does not exist or is not readable at the given location.
        """
        try:
            with open(path2file, "r") as infile:
                infile.read()
        except IOError:
            raise DemuxException("Could not open " + os.path.basename(path2file) + " at "
                                 + os.path.split(path2file)[0] + ".")
        return path2file

    def dir_check(self, path2dir):
        """
        Raises DemuxException if directory does not exist at the given location.
        """
        
        if (os.path.exists(path2dir)):
          return path2dir
        else:
          raise DemuxException("Could not find " + os.path.basename(path2dir) + " at " + os.path.split(path2dir)[0]) 
