import multiprocessing as mp
import os
import xml.etree.ElementTree as ET
import pandas as pd
import sys
import re
from IDeFIX.utilities import DemuxException
import pprint


class SampleSheet:
    """
    Takes Sample Sheet path for instantiation.
    Reads SampleSheet.csv of bcl2fastq version 1 and 2.
    Stores header information of SampleSheet as self.headerinfos (list) separately from the indextable as
    self.indextable (pandas.DataFrame). Stores a shortened Indextable containing the Sample IDs and Indices as
    self.indices_sampleID_df (pandas.DataFrame) and repetitions of indices/index combinations as self.index_repeats
    (dictionary; keys=repeating index(-combi), values=SampleIDs).
    """
    def __init__(self, samplesheet_path):
        self.path = samplesheet_path
        self.headerinfos, self.indextable = self.read_samplesheet_to_table()
        self.indices_sampleID_df = self.make_core_indextable()
        self.index_repeats = self.check_index_uniqueness()

    def read_samplesheet_to_table(self):
        """
        Reads SampleSheet.csv. All information above the index table are stored and returned as list of the
        lines/rows and the index table ist stored and returned as pandas.DataFrame.
        """
        samplesheet_headerinfos = []
        with open(self.path, "r") as csvfile:
            for line in csvfile:
                if not line.startswith("[Header]") and not line.startswith("FCID"):                 # if not bcl2fastq
                                                                                                    # v2 or v1
                    raise DemuxException("File Format Error: Sample Sheet. Neither the Sample Sheet of bcl2fastq "
                                         "version 1 nor that of version 2 was used.")
                elif line.startswith("FCID"):
                    header_list = line.split(",")
                    indextable = pd.read_csv(csvfile, delimiter=",", names=header_list)
                    if "-" in indextable.ix[0,"Index"]:     # if there are two indices (separated by a hyphen)
                        indextable["index"], indextable["index2"] = indextable["Index"].str.split("-").str
                    else:
                        indextable["index"] = indextable["Index"]
                    indextable.drop("Index", axis=1, inplace=True)
                    break
                elif line.startswith("[Header]"):
                    samplesheet_headerinfos.append(line)
                    for line in csvfile:
                        if not line.startswith("[Data]"):
                            samplesheet_headerinfos.append(line)
                        else:
                            samplesheet_headerinfos.append(line)
                            break
                    indextable = pd.read_csv(csvfile, delimiter=",")
                break
        return samplesheet_headerinfos, indextable

    def make_core_indextable(self):
        """
        Creates a core indextable pandas.DataFrame, consisting of the indices or index combinations, separated by "-",
        and the Sample ID from self.indextable. Returns this DataFrame.
        """
        if "index2" in list(self.indextable):               # in case there are two indices
            self.indextable["Indices"] = self.indextable[["index", "index2"]].apply(lambda x: "-".join(x), axis=1)
        else:  # in case there is only one index
            self.indextable["Indices"] = self.indextable[["index"]]
        core_indextable = self.indextable[["Indices", "Sample_ID"]]
        return core_indextable

    def check_index_uniqueness(self):
        """
        Checks if indices or index combinations are unique.
        Returns a dictionary of repeating indices/ index combinations with indices as keys and corresponding
        Sample IDs as values.
        """
        # to compare if indices are unique and if not store them in ununique dictionary; keys=indices, values=Sample IDs
        ununique = {}
        for i in range(len(self.indices_sampleID_df["Indices"])-1):
            for j in range(i+1, len(self.indices_sampleID_df)):
                # if indices are the same, but Sample IDs are not:
                if self.indices_sampleID_df.ix[i, "Indices"] == self.indices_sampleID_df.ix[j, "Indices"] and \
                        not self.indices_sampleID_df.ix[i, "Sample_ID"] == self.indices_sampleID_df.ix[j, "Sample_ID"]:
                    ununique[self.indices_sampleID_df.ix[i, "Indices"]] \
                        = [self.indices_sampleID_df.ix[i, "Sample_ID"], self.indices_sampleID_df.ix[j, "Sample_ID"]]
        return ununique

    # The following static methods pre-read the Sample Sheet and remove the unwanted characters \, ., ,, and tabs
    # as well as umlauts from the Sample Sheet prior to analysis.

    @staticmethod
    def swap(bases):
        out = ""
        transl = {'A':'T','T':'A','C':'G','G':'C'}
        for i in range(len(bases)):
            if (bases[i] == 'N'):
                out = out + 'N'
            else:
                out = transl[bases[i]] + out
        return out

    @staticmethod
    def reversecomplement(samplepath):
        output = ""
        samsh = open(samplepath, 'r')    
        x = samsh.readline()
        output = output + x

        # Skip header, while adding it to the output string

        while (x.startswith("[Data]")== False):
            x = samsh.readline()
            output = output + x
        #Data-Header / categories
        x = samsh.readline()
        output = output + x

            
        x=samsh.readline()
        while (x!=""):
                splitline = x.split(",")
                splitline[7] = SampleSheet.swap(splitline[7])
                output = output + ",".join(splitline)
                x=samsh.readline()
        samsh.close()
        samsh = open(samplepath+".rc", 'w')
        samsh.write(output)

    @staticmethod
    def read_samplesheet_linewise(samplesheet_path):
        """
        Takes Sample Sheet path and reads SampleSheet.csv linewise.
        Returns that list of lines.
        """
        samplesheet = []
        with open(samplesheet_path, "r") as csvfile:
            for line in csvfile:
                samplesheet.append(line)
        return samplesheet

    @staticmethod
    def check_and_replace_chars(samplesheet_list):
        """
        Takes a list of the lines of a Sample Sheet.
        Replaces unwanted sub-strings; backslashes, dots, tabs and whitespaces as well as umlauts.
        Returns the corrected list.
        """
        isHeader = True
        new_samplesheet = ""
        for line in samplesheet_list:
            if (isHeader):
                new_samplesheet = new_samplesheet + line
                if (line.startswith("Sample_ID")):
                    isHeader = False
            else:
                splitline = line.split(",")
                for i in [0,10]:
                    splitline[i] = re.sub('__+','_',re.sub('[^a-zA-Z0-9_\n-]',"_",splitline[i]))
                new_samplesheet += ",".join(splitline)
        return new_samplesheet

    #@staticmethod
    #def backup_original_samplesheet(samplesheet_list, projectpath):
    #    """
    #    Takes a list of lines of a Sample Sheet and the project path as arguments.
    #    Writes the list as SampleSheet_original.csv in the project path directory.
    #    """
    #    with open(os.path.join(projectpath, "SampleSheet_beforeIDeFIX.csv"), "w") as out_csvfile:
    #        for line in samplesheet_list:
    #            out_csvfile.write(line)

    @staticmethod
    def write_pre_corrected_samplesheet(samplesheet_list, outputpath):
        """
        Takes a list of lines of a Sample Sheet and the project path as arguments.
        Writes the list as SampleSheet.csv in the project path directory.
        """
        with open(outputpath, "w") as out_csvfile:
            for line in samplesheet_list:
                out_csvfile.write(line)


class RunInfo:
    """
    Takes Run Info path for instantiation.
    Reads RunInfo.xml.
    Extracts and stores the Instrument-ID as String, the Read List (containing the Read number, the amount of cycles
    and if the Read is an Index) as a list of Read dictionaries, and an Index ranges dictionary.
    """
    def __init__(self, runinfo_path):
        self.path = runinfo_path
        self.instrument, self.readlist = self.read_runinfo()
        self.indexranges = self.get_indexranges()
        self.device = self.get_device()

    def read_runinfo(self):
        """
        Reads RunInfo.xml.
        Returns Instrument-ID and Read List, containing the Read Number, the amount of cycles and if Read is an Index.
        """
        with open(self.path, "r") as xmlfile:
            runinfo = ET.parse(xmlfile).getroot()
        instrument = [node.text for node in runinfo.iter("Instrument")][0]      # stores instrument ID as string
        readlist = [i.attrib for i in runinfo.iter("Read")]  # stores bool of 'IsIndexed', int of 'Number' and
                                                             # 'NumCycles' as dictionary for each read in a List
        return instrument, readlist

    def get_indexranges(self):
        """
        Gets Index cycle ranges from self.readlist and returns them as an indexranges dictionary with i7/i5 as keys
        and corresponding cycle ranges as values.
        """
        # to check if Reads are in ascending order and if not, to sort them accordingly:
        readnumbercheck = [int(self.readlist[i]["Number"]) < int(self.readlist[i+1]["Number"]) for i in
                           range(len(self.readlist)-1)]
        if False in readnumbercheck:
            self.readlist = sorted(self.readlist, key=lambda k: k["Number"])
        # to fill the indexranges dictionary with the indices i7 and if existent i5 as keys and the corresponding cycle
        # ranges as values:
        cycle_start = 1
        cycle_end = 0
        indexlist = ["i7", "i5"]
        indexlist_index = 0
        indexranges = {}
        for read_dict in self.readlist:
            cycle_end += int(read_dict["NumCycles"])
            cyclerange = range(cycle_start, cycle_end + 1)
            cycle_start = cycle_end + 1
            if read_dict["IsIndexedRead"] == "Y":
                indexranges[indexlist[indexlist_index]] = cyclerange
                indexlist_index += 1
        return indexranges
    
    def get_device(self):
        
        # Reads the kind of device from the instrument ID.
        # 0 = miSeq
        # 1 = iSeq
        # 2 = nextSeq
        # 3 = undefined

        if (self.instrument.startswith("M")):
            return 0
        elif (self.instrument.startswith("FS")):
            return 1
        elif (self.instrument.startswith("NB")):
            return 2
        else:
            return 3

class FilterFile:
    """
    Takes a list of .filter file paths and a number of jobs/sub-processes for instantiation.
    Contains a filter dictionary {keys: strings of filter file names (w/o file extension), values: list of booleans of
    filtered clusters}, which is created by multiprocessed reading of the .filter files.
    """
    def __init__(self, filterfile_paths, jobs):
        self.path_list = filterfile_paths
        self.jobs = jobs
        self.filter_dict = self.get_filtered_clusters()

    def read_filterfile_job(self, filterfilepath):
        """
        Worker job that takes a path of a filter file as string.
        Reads binary filter files.
        Returns dictionary of one filter file with filename without file extension as key and list of booleans as value.
        """
        filter_dict0 = {}
        with open(os.path.join(filterfilepath), "rb") as f_file:
            header_zeroes = f_file.read(4)                  # reads first 4 bytes
            header_version = f_file.read(4)                 # reads second 4 bytes
            cluster_num = f_file.read(4)                    # reads third 4 bytes
            filtered_raw = f_file.read()
            clusters_bool = [x == 0 for x in filtered_raw]  # 0 equals bad quality = filtered => True
            # names of the filterfile without file extension:
            filter_dict0[os.path.splitext(os.path.basename(filterfilepath))[0]] = clusters_bool
        return filter_dict0

    def get_filtered_clusters(self):
        """
        Reads .filter files.
        Returns filter dictionary with truncated file name (without extension) as keys and list of booleans as
        values (True for filtered, False for passed cluster reads).
        """
        pool = mp.Pool(processes=self.jobs)
        filter_dict_list = pool.map(self.read_filterfile_job, self.path_list)
        pool.close()
        pool.join()
        filter_dict = {file: bool_list for dict_entry in filter_dict_list for file, bool_list in dict_entry.items()}
        return filter_dict
