import multiprocessing as mp
import os
import numpy as np
import pandas as pd
import gzip
import sys
from functools import partial
import pprint



from IDeFIX.utilities import DemuxException


class BclReader:
    """
    Takes a list of paths to lanes, an index ranges dictionary, a dictionary of .filter files, the number of
    jobs/sub-processes to be created and the threshold for minimum reads of indices to be reported for instantiation.
    Contains a .bcl dictionary {keys: tile file name (w/o file extension) as a string, values: nucleotide
    sequences of each index(-combination) of every cluster as list of lists of strings}
    and a pandas.DataFrame with a column containing the indices/ index combinations in descending order and a column
    with the count of those.
    """
    def __init__(self, lanepaths, indexranges, filter_dict, jobs, threshold_for_indexabundance):
        self.lanepaths = lanepaths      # list of paths to lanes
        self.indexranges = indexranges  # dictionary: keys: index ("i7", "i5"), values: range object of cycles
        self.filter_dict = filter_dict  # keys: .filter file name w/o extension | values: List of Booleans
                                        # True means filtered cluster and False passed cluster
        self.isMiseq = True
        self.jobs = jobs
        self.index_abundance_threshold = threshold_for_indexabundance           # minimum read number to be reported
        self.bcl_dict = self.get_nucseq_per_tile()          # keys: tile file, values: nucleotide sequence
        self.indexcombi_abundance_descending_df = self.find_abundances_of_indexcombis()
                                        # pandas.DataFrame (columns=["Existent Indices", "Quantity"]) with the indices/
                                        # index combinations in descending order


    def check_tilefile_format(self, path):
        """
        Checks if tiles are stored as .bcl file.
        Returns list of .bcl files.
        """
        if (os.path.exists(os.path.join(path, "C1.1")) == False):
            self.isMiseq = False
            unzipped_filename = [os.path.splitext(file)[0] for file in os.listdir(path)]
            tile_file_bcl_format = [os.path.splitext(filename)[1] for filename in unzipped_filename if filename.endswith(".bcl")]
        else:
            tile_file_bcl_format = [os.path.splitext(file)[1] for file in os.listdir(os.path.join(path, "C1.1")) if file.endswith(".bcl")]
        return tile_file_bcl_format

    def bits_to_base(self, byte):
        """
        Takes byte as binary, removes 3rd to 8th bit with a mask and returns base as string.
        """
        base_code = byte & 0b00000011
        if base_code == 0b00000000:
            return "A"
        if base_code == 0b00000001:
            return "C"
        if base_code == 0b00000010:
            return "G"
        if base_code == 0b00000011:
            return "T"
        else:
            print("byte parse error")

    def get_tilename_for_bclfile(self, bclfilename, lanenumber):
        """
        Get name for the FILTER-File. For miSeq-Data it is just the name of the BCL-File, for other Data it depends on the Lane-Name.
        """
        
        if self.isMiseq:
            return os.path.splitext(bclfilename)[0]
        else:
            return "s_{}".format(str(lanenumber))
        

    def is_zipped(self, filename):
        with open(filename, "rb") as file_to_check:
            
            a = file_to_check.read(4)
            if len(a) != 4:
                print("File too small.")
                sys.exit(1)
            if a == b"\x1f\x8b\x08\x04":
                return True
            else: 
                return False


    def read_bcl_job(self, cyclepaths_dict, filter_dict, bclfiles, tilefile, lanenumber = 2):
        """
        Takes a dictionary of cyclepaths {keys: indices ("i7" or "i5"), values: range object of cycles with index},
        dictionary of .filter files {keys: filterfile name, values: booleans of filtered clusters}
        and one tile file path.
        For each non-filtered cluster in that tile file, the nucleotide sequence corresponding to the indices are read.
        Returns a dictionary {keys: tile name (w/o the file extension), values: list of lists of strings of nucleotide
        sequences for each index(-combination) in every non-filtered cluster.
        """
#        tile_name = os.path.splitext(tilefile)[0]           # is the tile filename without the file extension
        tile_name = self.get_tilename_for_bclfile(tilefile, lanenumber)
        filtered_per_tile = filter_dict[tile_name]          # selects the specific tile processed in this worker job
        passed_clusters = filtered_per_tile.count(False)                        # returns the amount of good clusters
        # to define the overall amount of cycles (i7 + i5) for creating a matrix filled with zeros(int8):
        number_bases_to_read = 0
        if (self.isMiseq):
            for index in cyclepaths_dict.keys():                # for the indices "i7" and "i5"
                number_bases_to_read += len(cyclepaths_dict[index])                 # e.g. 2 x 8 bases = 16
        else:
            for index in cyclepaths_dict.keys():                # for the indices "i7" and "i5"
                number_bases_to_read += len(self.indexranges[index])                 # e.g. 2 x 8 bases = 16    
        index_array = np.zeros(dtype=np.int8, shape=(passed_clusters, number_bases_to_read))
        # to store the bases of high-quality (non-filtered) spots as integers in the matrix after reading the bcl-files:
        index_order = []                                    # stores which index is read in which order
        current_base = 0
        for index in cyclepaths_dict.keys():                # for the indices "i7" and "i5"
            index_order += [index]
            if (self.isMiseq):
                for cyclepath in cyclepaths_dict[index]:                            # for each cycle(-path) per index
                        filename = os.path.join(cyclepath, tilefile)
                        if self.is_zipped(filename):
                            with gzip.open(filename) as zippedfile:
                                header = zippedfile.read(4)
                                bcl_raw = zippedfile.read()
                        else:
                            with open(filename, "rb") as tile_file:
                                header = tile_file.read(4)
                                bcl_raw = tile_file.read()
                        assert len(bcl_raw) == len(filtered_per_tile)
                        # to store only the high-quality (not filtered) spots (=good_spot):
                        good_spot = 0
                        for spot in range(len(bcl_raw)):
                            if not filtered_per_tile[spot]:
                                index_array[good_spot][current_base] += ord(self.bits_to_base(bcl_raw[spot]))
                                good_spot += 1
                        current_base += 1
            else:
                    for i in self.indexranges[index]:
                        l = i-1
                        filename = os.path.join(cyclepaths_dict[index][0], bclfiles[l])
                        #print(filename)
                        if self.is_zipped(filename):
                            with gzip.open(filename) as zippedfile:
                                header = zippedfile.read(4)
                                bcl_raw = zippedfile.read()
                        else:
                            with open(filename, "rb") as tile_file:
                                header = tile_file.read(4)
                                bcl_raw = tile_file.read()
                        try:
                            assert len(bcl_raw) == len(filtered_per_tile)
                        except:
                            sys.exit(0)
                        # to store only the high-quality (not filtered) spots (=good_spot):
                        good_spot = 0
                        for spot in range(len(bcl_raw)):
                            if not filtered_per_tile[spot]:
                                index_array[good_spot][current_base] += ord(self.bits_to_base(bcl_raw[spot]))
                                good_spot += 1
                        current_base += 1
            
        # to get a new matrix with the bases as strings and indices separated from one another
        indexcombi_array = []           # structured like: [["spot1_i7" ,"spot1_i5"], ["spot2_i7", "spot2_i5"],...]
        for indexworm in index_array:
            spot_base_string = []
            current_base = 0
            for index in cyclepaths_dict.keys():            # for the indices "i7" and "i5"
                index_list = []
 
                if (self.isMiseq):
                    for cycle in cyclepaths_dict[index]:        # for each cycle(-path) per index
                        index_list += [chr(indexworm[current_base])]
                        current_base += 1
                else:    
                    for i in self.indexranges[index]:        # for each cycle(-path) per index
                        index_list += [chr(indexworm[current_base])]
                        current_base += 1
                spot_base_string += ["".join(index_list)]
            indexcombi_array += [spot_base_string]
        # to assure that the indices are stored in the right order (first "i7", then "i5"):
        if index_order[0] == "i5":      # if the first index is the i5 index, the order will be reversed to ["i7", "i5"]
            ordered_indexcombi = [list(reversed(index_combi)) for index_combi in indexcombi_array]
            bcl_dict0 = {tile_name: ordered_indexcombi}
        elif index_order[0] == "i7":
            bcl_dict0 = {tile_name: indexcombi_array}
        else:
            raise DemuxException("Problem occurred in BclReader.read_bcl_job():\n"
                                 "There is neither i7 nor i5 stored in the .bcl dictionary.")
        return bcl_dict0

    def get_nucseq_per_tile(self):
        """
        Performs a multiprocessed read of the cycles corresponding to the indices of each .bcl file.
        Returns them as a dictionary {keys: tile file name (w/o file extension) as a string, values: nucleotide
        sequences of each index(-combination) of every cluster as list of lists of strings}.
        """
        # checks if tiles are stored as bcl file or not
        if self.check_tilefile_format(self.lanepaths[0]):   # if not empty
            pass
        else:
            raise DemuxException("There are no bcl files to be found at " + str(self.lanepaths[0]))
        bcl_dict = {}
         
        
        for lanepath in self.lanepaths:
            lanenr = int(lanepath[-1])                      # assures that every cycle is assigned to the right lane

            # cyclepaths_dict: index:[cyclepaths] eg. "i7":["/path/of/cycleX", "path/of/cycleX+1",...], "i5":[...]
            cyclepaths_dict = {}
            for index in self.indexranges:
                if self.isMiseq:
                    cyclepaths_dict[index] = [os.path.join(lanepath, "C{}.1".format(cycle)) for cycle in     #FÜR ZIP AUFBEREITEN
                                          self.indexranges[index]]
                else:
                    cyclepaths_dict[index] = [str(lanepath)]
                    

            if self.isMiseq:
                bcl_files = [file for file in os.listdir(os.path.join(lanepath, "C1.1"))                    #FÜR ZIP AUFBEREITEN
                         if file.endswith(".bcl.bgzf") or file.endswith(".bcl")]          # stores all .bcl files for a lane
            else:
                bcl_files = [file for file in os.listdir(lanepath)                    #FÜR ZIP AUFBEREITEN
                         if file.endswith(".bcl.bgzf") or file.endswith(".bcl")]
                
            
            
            # multiprocessed read of bclfiles:
            pool = mp.Pool(processes=self.jobs)
            #f = open("input_read_bcl_job2.txt", "w")
            #for i in self.indexranges["i5"]:
            #    print(i)
            #for index in self.indexranges.keys():
            #    for i in self.indexranges[index]:
            #        print(bcl_files[i])
            #sys.exit(0)
            if (self.isMiseq):
                bcl_dict_list_per_lane = pool.map(partial(self.read_bcl_job, cyclepaths_dict, self.filter_dict, bcl_files), bcl_files)
            else:
                bcl_dict_list_per_lane = [self.read_bcl_job(cyclepaths_dict, self.filter_dict, bcl_files, bcl_files[0], lanenr)]
            
            pool.close()
            pool.join()
            # to remove the surrounding list:
            bcl_dict_per_lane = {file: bool_list for dict_entry in bcl_dict_list_per_lane
                    for file, bool_list in dict_entry.items()}
            bcl_dict.update(bcl_dict_per_lane)              # adds the new indices from bcl_files from this lane
            print("\n... PROGRESS ...\nBCL files of Lane {} were read".format(lanenr))

        unique_list = []
        
        # traverse for all elements 
        #for x in bcl_dict["s_1"]: 
        #    # check if exists in unique_list or not 
        #    if x not in unique_list: 
        #        unique_list.append(x)
        # print list 
        #for x in unique_list: 
        #    print(x)
        return bcl_dict

    def find_abundances_of_indexcombis(self):
        """
        Uses self.bcl_dict with tile names as keys and list of lists of index (["i7"]) or indices (["i7", "i5"])
        as values. Counts all indices/ indexcombis.
        Returns a pandas.Dataframe with a column containing the indices/ index combinations in descending order and a
        column with the count of those.
        """
        # to get if only one index or two must be stored in the index column, if two, then separate by hyphen
        num_indices = len(self.indexranges)
        indexcombi_variation_counter = 0
        indexcombi_abundance_dict = {}                      # keys: indexcombi, values: amount present
        # to count all indices/ index combinations:
        if num_indices == 1:            # if single indexing
            for tile_file in self.bcl_dict.keys():
                for index in self.bcl_dict[tile_file]:
                    if not index[0] in indexcombi_abundance_dict.keys():
                        indexcombi_abundance_dict[index[0]] = 1
                        indexcombi_variation_counter += 1
                    else:
                        indexcombi_abundance_dict[index[0]] += 1
        elif num_indices == 2:          # if dual indexing
            for tile_file in self.bcl_dict.keys():
                for index in self.bcl_dict[tile_file]:
                    indexcombo = index[0] + "-" + index[1]
                    if not indexcombo in indexcombi_abundance_dict.keys():
                        indexcombi_abundance_dict[indexcombo] = 1
                        indexcombi_variation_counter += 1
                    else:
                        indexcombi_abundance_dict[indexcombo] += 1
        else:
            raise DemuxException("The number of indices does not match either 1 or 2.\n"
                                 "Problem occurred in bclReader.find_abundances_of_indexcombis()")
        # clean up index combinations/indices below the user-specified abundance and create a DataFrame from the results
        indexcombi_abundance_cleaned_list = [(indexcombi, abundance) for (indexcombi, abundance)
                                             in indexcombi_abundance_dict.items()
                                             if abundance >= self.index_abundance_threshold]
        indexcombi_abundance_df = pd.DataFrame(indexcombi_abundance_cleaned_list,
                                               columns=["Existent Indices", "Quantity"])
        indexcombi_abundance_df_sorted = indexcombi_abundance_df.sort_values(by="Quantity", ascending=False)
        indexcombi_abundance_df_sorted = indexcombi_abundance_df_sorted.reset_index(drop=True)
        return indexcombi_abundance_df_sorted
